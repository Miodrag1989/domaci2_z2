﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci2_z2
{
    class Student
    {
        string ime;
        string prezime;
        string jmbg;
        List<float> prosecneOcene;
        float brojBodovaSaPrijemnog;
        float ukupanBrojbodova;

        public float Ukupno
        {
            get
            {
                return this.ukupanBrojbodova = (this.ukupanBrojbodova != 0) ? this.ukupanBrojbodova : this.prosecneOcene.Sum() * 2 + this.brojBodovaSaPrijemnog;
            }
        }

        public void Ucitavanje()
        {
            Console.Write("Ime: ");
            this.ime = Console.ReadLine();
            Console.Write("Prezime: ");
            this.prezime = Console.ReadLine();
            Console.Write("JMBG: ");
            this.jmbg = Console.ReadLine();
            Console.WriteLine("Prosecne ocene: ");
            this.prosecneOcene = new List<float>();
            for(int i = 0; i < 4; i++)
            {
                Console.Write($"Prosecna ocena iz {i + 1}. razreda: ");
                this.prosecneOcene.Add(float.Parse(Console.ReadLine()));
            }
            Console.Write("Broj bodova sa prijemnog: ");
            float.TryParse(Console.ReadLine(), out this.brojBodovaSaPrijemnog);
            Console.WriteLine();
        }

        public void Prikaz()
        {
            Console.WriteLine($"Student: {this.ime} {this.prezime} | {this.ukupanBrojbodova}");
            
        }
    }
}

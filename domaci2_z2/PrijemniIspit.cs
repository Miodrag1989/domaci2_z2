﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci2_z2
{
    class PrijemniIspit
    {
        int brojKandidata;
        Student[] studenti;
        int maxBrojStudenata;
        int prag;

        private void Sortiranje(ref Student[] studenti)
        {
            int imin = 0;
            for (int i = 0; i < studenti.Length - 1; i++)
            {
                imin = i;
                for (int j = i + 1; j < studenti.Length; j++)
                {
                    if ( studenti[imin].Ukupno < studenti[j].Ukupno)

                        imin = j;
                }
                if (i != imin)
                {
                    Student pom = studenti[imin];
                    studenti[imin] = studenti[i];
                    studenti[i] = pom;
                }
            }
        }
        public void Ucitavanje()
        {
            Console.Write("Maksimalni broj studenata za upis: ");
            int.TryParse(Console.ReadLine(), out this.maxBrojStudenata);
            Console.Write("Broj prijavljenih kandidata: ");
            int.TryParse(Console.ReadLine(), out this.brojKandidata);
            Console.Write("Prag prolaznosti: ");
            int.TryParse(Console.ReadLine(), out this.prag);
            Console.WriteLine("Upisivanje studenata!");
            this.studenti = new Student[this.brojKandidata];
            for(int i = 0; i < this.brojKandidata; i++)
            {
                this.studenti[i] = new Student();
                Console.WriteLine($"{i + 1}. student!");
                this.studenti[i].Ucitavanje();
            }
            Console.WriteLine();
        }

        public void Prikaz()
        {
            Sortiranje(ref studenti);
            Console.WriteLine("Rang lista!");
            for(int p = 0; p < this.maxBrojStudenata; p++)
            {
                if(studenti[p].Ukupno >= this.prag)
                    studenti[p].Prikaz();
            }
        }

        
       
    }
}
